// import { ApiRequest } from './apiRequest.js'

export class MainPage {

  constructor(container, ApiRequest) {
    this.client = container
    this.data = [{
      date : "loading...",
      temp : "loading...",
      pressure : "loading...",
      humidity : "loading..."
    }]
    ApiRequest.getConnection().then((data)=> this.load(data))
    this.render()
  }

  load(data_requested) {
    this.data = []
    data_requested.forEach(element => {
      this.data.push({
        date: element.data.date,
        temp: element.data.temp,
        pressure: element.data.pressure,
        humidity: element.data.humidity
      })
    })
    setTimeout(() => this.render(), 1000)
  }
  
  render() {
    this.client.innerHTML = `
    <div class="container">
      ${this.data.map(element => `
        <div class="thumbnail">
          <p>Date: ${element.date}</p>
          <p>Temperature: ${element.temp}</p>
          <p>Pressuer: ${element.pressure}</p>
          <p>Humidity: ${element.humidity}</p>
        </div>
      `)}
    </div>
    `
  }

}