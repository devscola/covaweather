export class ApiRequest {

  static async getConnection() {
    const api_url = `http://0.0.0.0:5000/get_forecasts`
    let response = await fetch(api_url)
    let payload = await response.json()
    console.log(payload)
    return payload
  }
}